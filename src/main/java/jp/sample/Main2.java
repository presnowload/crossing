package jp.sample;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 転倒数を使ってみた.
 * @author s.shimada
 *
 */
public class Main2 {
    private static final boolean DEBUG = false;
    private static final String TEXT = DEBUG ? "sample.txt" : "crossing.txt";

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        long inv = 0;

        BufferedReader b = null;
        try {
            FileReader fr = new FileReader(TEXT);
            b = new BufferedReader(fr);
            String s;
            List<Integer> points = new ArrayList<Integer>();
            while ((s = b.readLine()) != null) {
                Integer point = new Integer(s);

                for (Integer p : points) {
                    if (point < p) {
                        inv++;
                    }
                }

                points.add(point);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                b.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        print(inv, TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime));

    }

    private static void print(long count, long seconds) {
        System.out.println(count + "," + seconds);
        System.out.println("ENV: Java");
    }

}
