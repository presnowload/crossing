package jp.sample;

/**
 * y = ax + b;
 */
public class Line {
    public double a;
    public double b;

    public Line(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public boolean hasInterSection(Line line) {
        if (a - line.a == 0)
            return false;

        double ans = (-b + line.b) / (a - line.a);

        if (0 < ans && ans < 1)
            return true;

        return false;
    }
}
