package jp.sample;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MargeSortMain {

    private static long resultVal;
    private static final boolean DEBUG = false;
    private static final String TEXT = DEBUG ? "sample.txt" : "crossing.txt";

    /**
     * @param args
     */
    public static void main(String[] args) {
        long startTimeMillis = System.currentTimeMillis();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(TEXT));

            String fileData;
            int intData = 0;
            List<Integer> list = new ArrayList<Integer>();

            while ((fileData = br.readLine()) != null) {
                intData = Integer.parseInt(fileData);
                list.add(intData);
                //list = mergeSort(list, intData);
            }
            list = mergeSort(list, intData);

            //System.out.println("list:" + list.toString() + "");

            System.out.println("結果 = " + resultVal);
        } catch (Exception e) {
            System.out.println("ファイル読み込み失敗");
        } finally {
            try {
                br.close();
            } catch (IOException e) {

            }
        }
        System.out.println("実行時間(sec) = "
            + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTimeMillis));
        System.out
            .println("実行時間(ms) = " + TimeUnit.MILLISECONDS.toMillis(System.currentTimeMillis() - startTimeMillis));
    }

    private static List<Integer> mergeSort(List<Integer> list, int intData) {

        List<Integer> a1 = new ArrayList<Integer>();
        List<Integer> a2 = new ArrayList<Integer>();

        if (list.size() > 1) {
            int m = list.size() / 2;
            int n = list.size() - m;
            for (int i = 0; i < m; i++) {
                a1.add(list.get(i));
            }
            for (int i = 0; i < n; i++) {
                a2.add(list.get(m + i));
            }
            a1 = mergeSort(a1, intData);
            a2 = mergeSort(a2, intData);
            list = merge(a1, a2, intData);
        }
        return list;
    }

    private static List<Integer> merge(List<Integer> a1, List<Integer> a2, int intData) {
        int i = 0, j = 0;
        List<Integer> listTmp = new ArrayList<Integer>();
        while (i < a1.size() || j < a2.size()) {
            if (j >= a2.size() || (i < a1.size() && a1.get(i) < a2.get(j))) {
                listTmp.add((i + j), a1.get(i));
                i++;
            } else {
                listTmp.add((i + j), a2.get(j));
                //                if (i < a1.size() && a2.get(j) == intData) {
                resultVal = resultVal + a1.size() - i;
                //                }
                j++;

            }
        }
        return listTmp;
    }
}
