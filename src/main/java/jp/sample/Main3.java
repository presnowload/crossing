package jp.sample;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Main3 {
    private static final boolean DEBUG = false;
    private static final String TEXT = DEBUG ? "sample.txt" : "crossing.txt";

    /**
     * 転倒数を２分探索木に追加するように改良.
     * @param args
     */
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        long inv = 0;

        BufferedReader b = null;
        try {
            FileReader fr = new FileReader(TEXT);
            b = new BufferedReader(fr);
            String s;
            BinarySearchTree points = null;

            while ((s = b.readLine()) != null) {
                if (null != points) {
                    inv += points.add(Integer.parseInt(s));
                } else {
                    points = new BinarySearchTree(Integer.parseInt(s), null);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                b.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        print(inv, startTime);

    }

    private static void print(long count, long startTime) {
        System.out.println(count + "," + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime));
        System.out.println("ENV: Java");
    }

}
