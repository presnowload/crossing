package jp.sample;

public class BinarySearchTree {
    public int count;
    public int value;
    public BinarySearchTree left;
    public BinarySearchTree right;
    public BinarySearchTree parent;

    public BinarySearchTree(int value, BinarySearchTree parent) {
        this.value = value;
        this.count = 1;
        this.left = null;
        this.right = null;
        this.parent = parent;
    }

    public int add(int value) {

        int inv = 0;
        if (null == this.parent && 0 == this.value) {
            // self
            this.value = value;
        } else if (this.value < value) {
            // add right
            this.count++;
            if (null == this.right) {
                this.right = new BinarySearchTree(value, this);
            } else {
                inv += this.right.add(value);
            }

        } else if (value < this.value) {
            // add left
            inv += this.count;
            if (null == this.left) {
                this.left = new BinarySearchTree(value, this);
            } else {
                inv += this.left.add(value);
            }
        } else {
            // equal
            this.value = value;
        }

        return inv;
    }

    public BinarySearchTree search(int value) {
        if (this.value == value)
            return this;
        if (this.value < value && this.right != null)
            return this.right.search(value);
        if (value < this.value && this.left != null)
            return this.left.search(value);
        return null;
    }

}
