package jp.sample;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main {

    private static final boolean DEBUG = false;

    /**
     * 0 <= x <= 1 上の 線分として計算してみた.
     * @param args
     */
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        long interSectionCount = 0;

        String text = DEBUG ? "sample.txt" : "crossing.txt";

        BufferedReader b = null;
        try {
            FileReader fr = new FileReader(text);
            b = new BufferedReader(fr);
            String s;
            int y0 = 1;

            List<Line> lines = new ArrayList<Line>();
            while ((s = b.readLine()) != null) {
                int y1 = Integer.parseInt(s);
                int dy = y1 - y0;
                int dx = 1 - 0;
                int slope = dy / dx;
                Line line = new Line(slope, y0);

                for (Line l : lines) {
                    if (l.hasInterSection(line)) {
                        interSectionCount++;
                    }
                }

                //                System.out.println(y0 + ":" + s);

                lines.add(line);
                y0++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                b.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println(interSectionCount + ","
            + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime));

    }
}
